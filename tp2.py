class Box():
    def __init__(self, is_open=False, capacity=None):
        self._cont=[]
        self._is_open=False
        self._capacity=None

    def __contains__(self,truc):
        return truc in self._cont

    def add(self,truc):
        self._cont.append(truc)
        if self._capacity==None:
            pass
        else:
            self._capacity-=truc.volume()

    def sup(self,truc):
        self._cont.remove(truc)

    def is_open(self):
        return self._is_open

    def open(self):
        self._is_open=True

    def close(self):
        self._is_open=False

    def action_look(self):
        if self._is_open:
            str1="la boite contient: "
            str1+=", ".join(self._cont)
        else:
            str1="la boite est fermee"
        return str1

    def set_capacity(self,nb):
        self._capacity=nb
    
    def capacity(self):
        return self._capacity

    def has_room_for(self,thing):
        return self._capacity==None or thing.volume()<=self._capacity

    def action_add(self,thing):
        if self.has_room_for(thing) and self._is_open:
            self.add(thing)
            return True
        return False

    def find(self,nom):
        if not self._is_open:
            return
        for thing in self._cont:
            if thing.has_name(nom):
                return thing
        return None

class Thing():

    def __repr__(self):
        return self._nom

    def __init__(self,poid,nom=None):
        self._poid=poid
        self._nom=nom

    def volume(self):
        return self._poid

    def get_name(self):
        return self._nom
    
    def has_name(self,nom):
        return nom==self._nom

    def set_name(self,nom):
        self._nom=nom